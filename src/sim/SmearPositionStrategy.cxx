#include "MuonSim/SmearPositionStrategy.h"

namespace MuonSim {
  /*! \brief Constructor
   * Call MCTruthRecoStrategy constructor    
   */
  SmearPositionStrategy::SmearPositionStrategy(MuonReco::ConfigParser cp) : GasMonitorRTStrategy(cp) {}

  /*! \brief Implementation of ReconstructionStrategy interface
   * 
   * Add gaussian noise to truth position.  For truth position calculation, see MCTruthRecoStrategy::addRecoHit
   */
  void SmearPositionStrategy::addRecoHit(std::vector<G4DriftTubeHit*> truthHits) {
    if (truthHits.size() == 0) return;

    G4double minDistance = getMinHitDistance(truthHits);
    
    double hx, hy;
    G4double hr, ht;
    hr = minDistance/CLHEP::mm + MuonReco::Hit::RadiusError(minDistance/CLHEP::mm)*G4RandGauss::shoot(0,1);
    ht = findHitTime(hr);;

    geo.GetHitXY(truthHits.at(0)->GetLayer(), truthHits.at(0)->GetColumn(), &hx, &hy);
    MuonReco::Hit h = MuonReco::Hit(ht/CLHEP::ns, ht/CLHEP::ns,
                                    ht/CLHEP::ns, ht/CLHEP::ns,
				    0, 0, truthHits.at(0)->GetLayer(), truthHits.at(0)->GetColumn(), hx, hy);
    // add radius dependent gaussian noise to the hit radius
    h.SetRadius(hr);
    evt.AddSignalHit(h);
  }

}
